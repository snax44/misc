#!/bin/bash
###
# Author: Mael
# Date: 20230302
# Desc:
#  For each domain listed in given file as $2, the script make a dig looking for A Record.
#  If no A record is found same test is done with CNAME
#  Domains without A nor CNAME is printed in STDOUT
#  At the end number of tested and failed domains are printed
###

DNS_SERVER="$1"
LIST="$2"
COUNTER_TESTED=0
COUNTER_DEAD=0

function msg(){
  # Call this function to print a beautifull colored message
  # Ex: msg ko "This is an error"

  local GREEN=\\033[1;32m
  local NORMAL=\\033[0;39m
  local RED=\\033[1;31m
  local PINK=\\033[1;35m
  local BLUE=\\033[1;34m
  local WHITE=\\033[0;02m
  local YELLOW=\\033[1;33m

  if [ "$1" == "ok" ]; then
    echo -e "[$GREEN  OK  $NORMAL] $2"
  elif [ "$1" == "ko" ]; then
    echo -e "[$RED ERROR $NORMAL] $2"
  elif [ "$1" == "warn" ]; then
    echo -e "[$YELLOW WARN $NORMAL] $2"
  elif [ "$1" == "info" ]; then
    echo -e "[$BLUE INFO $NORMAL] $2"
  fi
}


if [[ "$DNS_SERVER" == "" ]];then
  msg ko "Please add the DNS Server ip as 1st argument"
  exit 1
elif [[ "$LIST" == "" ]];then
  msg ko "Please add the filename containing domain to test as 2nd argument"
  exit 1
fi

for domain in $(cat $LIST); do
  A_RECORD=$(dig A $domain @$DNS_SERVER +short)
  let COUNTER_TESTED++

  if [[ "$A_RECORD" != ""  ]];then
    true
  else
    CNAME_RECORD=$(dig CNAME $domain @$DNS_SERVER +short)
    if [[ "$CNAME_RECORD" == ""  ]];then
      echo "$domain"
      let COUNTER_DEAD++
    fi
  fi
done

echo "=================================================================="
msg info "$COUNTER_TESTED tested domains, $COUNTER_DEAD dead domains ($(($COUNTER_DEAD*100/$COUNTER_TESTED))%)"