#!/bin/bash

# Get the default route with the lowest metric
interface=$(ip route show default | sort -k5 -n | head -n1 | awk '{print $5}')

# Check if an interface was found
if [ -n "$interface" ]; then
#    echo "Using interface: $interface"
    # Fetch public IP using the selected interface
    public_ip=$(curl --interface $interface -s ifconfig.me)
#    echo "Public IP: $public_ip"
    echo "$public_ip"
else
    echo "No default route found!"
    exit 1
fi