<?php
// WHOIS Lookup Handler
if (isset($_GET['whois'])) {
    $ip = filter_var($_GET['whois'], FILTER_VALIDATE_IP);
    if ($ip) {
        // Use shell's `whois` command (ensure `whois` is installed)
        $output = shell_exec("whois " . escapeshellarg($ip));
        header('Content-Type: text/plain');
        echo $output ?: "No WHOIS data available for $ip.";
    } else {
        echo "Invalid IP address.";
    }
    exit;
}

// DNS Query Handler
if (isset($_GET['dns_query'])) {
    $domain = filter_var($_GET['dns_query'], FILTER_SANITIZE_STRING);
    $customDns = isset($_GET['custom_dns']) ? filter_var($_GET['custom_dns'], FILTER_VALIDATE_IP) : null;

if ($domain) {
    // Use the custom DNS server if provided
    $dnsCmd = $customDns ? "dig @$customDns" : "dig";
    $dnsOutput = shell_exec("$dnsCmd +noall +answer " . escapeshellarg($domain));

    // Get full details
    $dnsDetails = shell_exec("$dnsCmd " . escapeshellarg($domain));

    header('Content-Type: application/json');
    echo json_encode([
        'short_output' => $dnsOutput,
        'full_output' => $dnsDetails
    ]);

    } else {
        echo "Invalid domain.";
    }
    exit;
}

// Function to get the country code for an IP
function getCountryCode($ip) {
    $apiUrl = "http://ip-api.com/json/" . urlencode($ip);
    $response = @file_get_contents($apiUrl);
    $data = json_decode($response, true);
    return $data['countryCode'] ?? 'Unknown';
}

// Variable for dynamic title
$title = "Example DDNS Status";  // You can change this to any value or even fetch it from a configuration file or environment variable

// Array of domains for testing
$domains = [
  "example.be",
  "example.it",
  "example.de",
  "example.com"
];

$results = [];

// Resolve each domain's current IP and country
foreach ($domains as $domain) {
    $ip = gethostbyname($domain);
    $countryCode = getCountryCode($ip);
    $results[] = [
        'domain' => $domain,
        'ip' => $ip,
        'countryCode' => $countryCode
    ];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo htmlspecialchars($title); ?></title>
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #121212;
            color: #e0e0e0;
            margin: 0;
            padding: 0;
        }
        h1 {
            text-align: center;
            color: #ffffff;
            padding: 20px;
            background-color: #1e1e1e;
            margin: 0;
            border-bottom: 2px solid #333;
        }
        .container {
            max-width: 800px;
            margin: 20px auto;
            padding: 20px;
            background: #1e1e1e;
            border-radius: 15px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.5);
        }
        .domain-card {
            background: #242424;
            margin: 10px 0;
            padding: 15px;
            border-radius: 10px;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        .domain-card h2 {
            margin: 0;
            font-size: 1.2em;
            color: #64b5f6;
            cursor: pointer;
            text-decoration: underline;
        }
        .domain-card .ip-container {
            display: flex;
            align-items: center;
        }
        .domain-card .ip-container img {
            width: 20px;
            height: auto;
            margin-right: 10px;
        }
        .domain-card .ip-container p {
            margin: 0;
            font-size: 1em;
            color: #03dac6;
            cursor: pointer;
            text-decoration: underline;
        }
        footer {
            text-align: center;
            margin-top: 20px;
            color: #aaaaaa;
        }
        footer p {
            margin: 0;
        }
        #whoisModal, #dnsModal {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 80%;
            max-width: 600px;
            background: #1e1e1e;
            color: #e0e0e0;
            padding: 20px;
            border-radius: 15px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.7);
            display: none;
            z-index: 1000;
        }
        #whoisModal pre, #dnsModal pre {
            overflow: auto;
            max-height: 400px;
            background: #242424;
            padding: 10px;
            border-radius: 10px;
        }
        #modalBackdrop {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
            display: none;
            z-index: 999;
        }
        #closeModal {
            margin-top: 10px;
            padding: 10px 20px;
            background: #03dac6;
            color: #000;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        .dns-input {
            margin-top: 20px;
            padding: 10px;
            background: #242424;
            border-radius: 10px;
            text-align: center;
        }
        .dns-input label {
            margin-right: 10px;
            font-size: 1em;
            color: #ffffff;
        }
        .dns-input input {
            padding: 5px 10px;
            border: 1px solid #333;
            border-radius: 5px;
            background: #121212;
            color: #e0e0e0;
        }
        .dns-input button {
            margin-left: 10px;
            padding: 5px 15px;
            background: #03dac6;
            border: none;
            border-radius: 5px;
            color: #000;
            cursor: pointer;
        }
        .dns-input button:hover {
            background: #029e84;
        }

    </style>
</head>
<body>
    <h1><?php echo htmlspecialchars($title); ?></h1>
    <div class="container">
        <?php foreach ($results as $entry): ?>
            <div class="domain-card">
                <h2 onclick="getDnsQuery('<?= htmlspecialchars($entry['domain']); ?>')"><?= htmlspecialchars($entry['domain']); ?></h2>
                <div class="ip-container">
                    <img src="https://flagcdn.com/w20/<?= strtolower($entry['countryCode']); ?>.png" alt="<?= htmlspecialchars($entry['countryCode']); ?>" title="<?= htmlspecialchars($entry['countryCode']); ?>">
                    <p onclick="getWhois('<?= htmlspecialchars($entry['ip']); ?>')"><?= htmlspecialchars($entry['ip']); ?></p>
                </div>
            </div>
        <?php endforeach; ?>
        <div class="dns-input">
            <label for="customDns">Custom DNS Server (Optional):</label>
            <input type="text" id="customDns" placeholder="e.g., 8.8.8.8">
            <button onclick="refreshWithDns()">Set DNS and Refresh</button>
        </div>
    </div>
    <footer>
        <p>Updated at <?= date('Y-m-d H:i:s'); ?></p>
	<p>Current: <?= $_SERVER['REMOTE_ADDR']; ?></p>
    </footer>

    <!-- WHOIS Modal -->
    <div id="modalBackdrop" onclick="closeModal()"></div>
    <div id="whoisModal">
        <h2>WHOIS Information</h2>
        <pre id="whoisContent">Loading...</pre>
        <button id="closeModal" onclick="closeModal()">Close</button>
    </div>

    <!-- DNS Query Modal -->
    <div id="dnsModal">
        <h2>DNS Query Results</h2>
        <pre id="dnsContent">Loading...</pre>
        <button id="closeModal" onclick="closeModal()">Close</button>
    </div>

    <script>
        function getWhois(ip) {
            const modal = document.getElementById('whoisModal');
            const content = document.getElementById('whoisContent');
            const backdrop = document.getElementById('modalBackdrop');
            modal.style.display = 'block';
            backdrop.style.display = 'block';

            fetch(`?whois=${ip}`)
                .then(response => response.text())
                .then(data => content.textContent = data)
                .catch(error => content.textContent = `Error: ${error}`);
        }

        function getDnsQuery(domain) {
            const modal = document.getElementById('dnsModal');
            const content = document.getElementById('dnsContent');
            const backdrop = document.getElementById('modalBackdrop');
            modal.style.display = 'block';
            backdrop.style.display = 'block';
        
            // Get the custom DNS server from the URL (if any)
            const params = new URLSearchParams(window.location.search);
            const customDns = params.get('custom_dns');
        
            // Build the fetch URL
            let fetchUrl = `?dns_query=${encodeURIComponent(domain)}`;
            if (customDns) {
                fetchUrl += `&custom_dns=${encodeURIComponent(customDns)}`;
            }
        
            fetch(fetchUrl)
                .then(response => response.json())
                .then(data => {
                    const formattedDnsData = formatDnsOutput(data.full_output);
                    content.textContent = formattedDnsData;
                })
                .catch(error => content.textContent = `Error: ${error}`);
        }

        function formatDnsOutput(dnsData) {
            return dnsData.replace(/\n/g, '\n');
        }

        function closeModal() {
            document.getElementById('whoisModal').style.display = 'none';
            document.getElementById('dnsModal').style.display = 'none';
            document.getElementById('modalBackdrop').style.display = 'none';
        }
        function refreshWithDns() {
            const customDns = document.getElementById('customDns').value.trim();
            const params = new URLSearchParams(window.location.search);
            if (customDns) {
                params.set('custom_dns', customDns);
            } else {
                params.delete('custom_dns');
            }
            window.location.search = params.toString();
        }
    </script>
</body>
</html>